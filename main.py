from scripts.table import subtables
from pathlib import Path

if __name__ == "__main__":

    session_active = True

    while session_active:
        configuratie = input("\nVoer pad naar configuratie JSON-bestand in of typ \'exit\' om af te sluiten:\n\n")

        if configuratie=='exit':
            sessions_active = False
            print("\nBodemdalingsscripts worden afgesloten...")
            break
        elif Path(configuratie).is_file():
            c = subtables(configuratie)
            c.update_table()
            print("Tabel opslaan")
            c.export_table()
            print("Klaar!")
        else:
            print("Pad naar configuratiebestand niet herkend, probeer nogmaals.")