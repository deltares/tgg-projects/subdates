# Subdates

Subdates is een Python script om bodemdalingsvolumes te updaten voor zones langs de kust. 

## Wat doet het script?

Subdates berekent de volumeverandering voor gegeven deelgebieden op basis van rasterbestanden die elk een component van de totale bodembeweging beschrijven. Een raster van een bodembewegingsscomponent wordt gegeven in een lengte-eenheid per tijdseenheid. Bijvoorbeeld: "Bodemdaling door gaswinning in centimeter per 30 jaar."

Input:

- Polygonen shapefile met deelgebieden
- Rasters met bodembewegingcomponenten (zoals gaswinning, tektoniek, glacio-isostasie, etc.). De rasters moeten in een bekend coordinatensysteem staan.
- Een configuratiebestand in JSON format dat de in/uitvoerpaden, eenheden, onzekerheden en andere opties geeft

Output:

- Een csv-bestand met volumeveranderingen per bodembewegingscomponent en eventueel indien gewenst cumulatieve volumeveranderingen (volumeveranderingen opgeteld).


## Hoe gebruik je het?
**Stap 0: Installeren**

Er is een requirements.txt gegeven om de vereiste packages te installeren in je Python-environment


**Stap 1: configuratiebestand maken**

Het configuratiebestand bevat de volgende gegevens die vantevoren ingevoerd moeten worden:

- *gebied*: Pad naar polygonen shapefile met deelgebieden
- *uitvoer*: Pad naar te produceren csv-bestand met resultaten
- *kolomnaam_volgorde*: Naam van de shapefilekolom waarin de gebiedsnummering staat. Deze volgorde wordt aangehouden in de uitvoer.
- *dst_crs*: EPSG code van het coordinatensysteem waarin gewerkt dient te worden. Rasters in een andere coordinatensysteem worden automatisch hiernaar omgezet.
- *grid*: Grid waarop alle rasters worden geprojecteerd, gegeven in een Python list met de volgende waarden: \[Xmin, Xmax, Ymin, Ymax, Resolutie\]. Let op dat de coordinaten kloppen met dst_crs.
- *combinaties*: Welke combinaties van bodembewegingscomponenten er gemaakt worden. Gegeven in Python dictionary, waarin de 'keys' de kolomnamen zijn van de combinaties. De 'values' zijn Python lists die corresponderen met rasters die onder *rasters* gegeven zijn (zie hieronder). Dus \[0, 1, 0, 1\] telt rasters 2 en 4 bij elkaar op.
- *rasters*: Verzameling van rasters en gegevens daarvan. Voor elk bestand moet het volgende opgegeven worden:
	- *locatie van rasterbestand*: Pad naar locatie van rasterbestand
	- *onzekerheid als fractie*: Onzekerheid als fractie van rasterwaarden
	- *periode in jaren*: Periode in jaren waarvoor het raster een bodemhoogteverandering geeft
	- *gegeven eenheid*: Hoe de lengteeenheid is gegeven: "-2 m" betekent centimeter, "3 m" betekent kilometer etc.
	- *tabel eenheid*: Gewenste volumeenheid: "6 m" betekent dat het resultaat wordt gegeven in 10<sup>6</sup> m<sup>3</sup> (miljoen kuub)

<img title="Configuratiebestand" alt="" src="https://gitlab.com/deltares/tgg-projects/subdates/-/raw/main/docs/config.PNG">

**Stap 2: draaien van script**

Open in Windows het command prompt of een IDE en draai 'main.py' in Python

Je wordt gevraagd een pad naar het configuratiebestand te geven. Voer dit pad in en druk op enter. De berekeningen worden nu uitgevoerd, als alles goed gaat krijg je de volgende meldingen te zien:

<img title="Configuratiebestand" alt="" src="https://gitlab.com/deltares/tgg-projects/subdates/-/raw/main/docs/gedraaid_voorbeeld.PNG">

Voer 'exit' in als je af wilt sluiten.
