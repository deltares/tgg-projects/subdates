from pathlib import Path
import numpy as np
import imod
import xarray as xr
import pandas as pd
import geopandas as gpd
# from shapely.geometry import Polygon
# from shapely.ops import unary_union
from tqdm import tqdm
import json
# import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings('ignore')

class subtables():
    
    def __init__(self, config_file):
        with open(config_file) as fobj:
            config = json.load(fobj)
            print("\nConfiguratie geldig")
         
        print("Gebieden shapefile inlezen...")
        self.gdf = gpd.read_file(Path(config['gebied']))
        try:
            self.gdf.sort_values(by=config['kolomnaam_volgorde'], inplace=True)
        except KeyError:
            print('Column name for row order does not exist, default order used')
        
        self.grid = self.__target_grid(config['grid'])   
        self.regridder = imod.prepare.Regridder(method='nearest')   
            
        self.rasters = {}
        for key in tqdm(config['rasters'].keys(), desc='Raster inlezen en herprojecteren...\n'):
            self.rasters[key] = [self.__read_project_adjust(Path(config['rasters'][key]['locatie van rasterbestand']), int(config['dst_crs'])), 
                                 config['rasters'][key]['onzekerheid als fractie'],
                                 config['rasters'][key]['periode in jaren'],
                                 config['rasters'][key]['gegeven eenheid'],
                                 config['rasters'][key]['tabel eenheid']]
        
        self.resolutions = []
        self.unc_scalars = []
        for rast in self.rasters.values():
            self.resolutions.append(rast[0].attrs['res'][0])
            self.unc_scalars.append(rast[1])
        
        idx = np.argmin(np.array([p[1] for p in self.rasters.values()]))
        self.like = [k for k in self.rasters.keys()][idx]
        
        self.combis = config['combinaties']
        
        self.table = None
        self.expath = Path(config['uitvoer'])
        
        
    def update_table(self):
        """
        

        Returns
        -------
        None.

        """       
        
        series = []
                
        for _, location in tqdm(self.gdf.iterrows(), desc="Tabel update voor deelgebied\n"):
                        
            if location.geometry.geom_type == 'MultiPolygon':
                index = [i for i in range(len(location.geometry))]                
            else:
                index = [0]
                
            shp = gpd.GeoDataFrame({'id': 0, 'geometry': location[-1]}, index=index)
            shape_raster = imod.prepare.rasterize(shp, self.rasters[self.like][0])
            shape_raster = self.__slice_to_finite_values(shape_raster)
            
            arrays = []
            volumes = []
            uncertainties = []
            scalars = []
            serie = pd.Series({'Gebied': location['Gebied'],})
            for i, raster in enumerate(self.rasters):            
                # Regrid to shape_raster and compute volume
                arrays.append(self.regridder.regrid(self.rasters[raster][0], shape_raster) * shape_raster)
                volume, scalar = self.__get_volume(arrays[i], location.geometry.area, self.rasters[raster][2], self.rasters[raster][3], self.rasters[raster][4])
                volumes.append(volume)
                uncertainties.append(np.abs(volume * self.unc_scalars[i]))
                scalars.append(scalar)
                
                serie[raster] = f'{np.round(volumes[i], 3)} ' + '\xb1' + f' {np.round(uncertainties[i], 3)}'
                          
            # Combinations
            try:
                n_combis = len(self.combis)
                for j, key in enumerate(self.combis):
                    if len(self.combis[key]) == len(self.rasters):                    
                        data = [d for d, c in zip(arrays, self.combis[key]) if c!=0]
                        ucs = [d for d, c in zip(self.unc_scalars, self.combis[key]) if c!=0]
                        us = [d for d, c in zip(scalars, self.combis[key]) if c!=0]
                        vols = [d for d, c in zip(volumes, self.combis[key]) if c!=0]
                                        
                        comb_unc = np.sqrt(self.__arraysum(data, scalars=np.array(ucs/np.array(us)), power=2))     
                        total_unc = np.nanmean(comb_unc)*location.geometry.area
                        volumes.append(sum(vols))
                        uncertainties.append(total_unc)
                    else:
                        print(f'Combination \'{key}\' not understood, can not add this combination')
                        continue
    
                    serie[key] = f'{np.round(volumes[-1], 3)} ' + '\xb1' + f' {np.round(uncertainties[-1], 3)}'    
            except IndexError:
                print('Note: No combinations given')
        
            series.append(serie)
            
        self.table = pd.concat(series, axis=1).transpose()
    
    def export_table(self):
        if any(self.table):
            self.table.to_csv(self.expath, encoding='utf-8')
    
    def __get_volume(self, array, surface_area, period, unit_string_or, unit_string_target):
        scalar = self.__determine_unit(unit_string_or, unit_string_target, period)
        return(np.nanmean(array) * surface_area / scalar, scalar)
    
    
    def __read_project_adjust(self, raster_path, crs_dst): 
        """
        method to read, reproject and adjust a raster given as path 
        string or WindowsPath object, then return as xarray.DataArray
        that is casted to a target grid.
        
        Parameters
        ----------
        raster_path : string or WindowsPath object
            Path to raster.
        crs_dst : int
            epsg number as integer. Raster will be reprojected to this coordinate system

        Returns
        -------
        xarray.DataArray

        """
        
        if Path(raster_path).is_dir():
            p = Path(raster_path).joinpath('w001001.adf')
        else:
            p = Path(raster_path)
        
        raster = xr.open_rasterio(p).squeeze()
        crs = raster.attrs['crs']
        nodata = raster.attrs['nodatavals']
        raster = xr.DataArray(coords = {'y': raster.y, 'x': raster.x},
                              dims = ['y', 'x'], 
                              data = raster.data
                                        ).astype(np.float32)
            
        
        if crs != {'init': f'EPSG:{str(crs_dst)}'}:
            grid_adjusted = imod.prepare.reproject(self.grid, src_crs={'init':f'EPSG:{str(crs_dst)}'}, dst_crs=crs)
            raster = self.regridder.regrid(raster, grid_adjusted)
            
            if raster.y[0] < raster.y[1]:
                raster = raster[::-1,:]
            
            raster = imod.prepare.reproject(raster, src_crs=crs, dst_crs={'init':f'EPSG:{str(crs_dst)}'})
        
        raster = raster.where(raster!=nodata, other=np.nan)
        
        return(raster)
    
    @staticmethod   
    def __target_grid(geometry):       
        minx = geometry[0]
        maxx = geometry[1]
        miny = geometry[2]
        maxy = geometry[3]
        like = xr.DataArray(coords = {
                                        'y': np.arange(maxy, miny, -geometry[-1]),
                                        'x': np.arange(minx, maxx, geometry[-1])
                                        
                                      },
                            dims = ['y', 'x']
                            ).astype(np.float32) 
        return(like)
    
    @staticmethod
    def __slice_to_finite_values(array):
        nonna = array.where(xr.ufuncs.isfinite(array), drop=True)
        if nonna.any():
            minx = nonna.x.min().values; maxx = nonna.x.max().values
            maxy = nonna.y.max().values; miny = nonna.y.min().values
            return(array.sel(x=slice(minx, maxx), y=slice(maxy, miny)))
        else:
            return(array)
        
    @staticmethod
    def __arraysum(arrays, scalars=[], power=1):
        """
        Index-wise sum of a list of arrays

        Parameters
        ----------
        arrays : list
            list of xr.DataArrays or np.Arrays
        scalars : list
            A list of real numbers that can be used to manipulate the rasters in
            the arrays list. The default is [] (all scalars will be 1).
        power : real number
            power to manipulate each array in arrays list with. e.g. power 2:
            sum(array_1**2 + array_2**2 + ... + array_n**2). The default is 1.

        Returns
        -------
        None.

        """
        sumarray = np.zeros_like(arrays[0])
        if len(scalars) != len(arrays):
            print('WARNING: Number of scalars and arrays mismatch, all scalars set to 1')
            np.ones(len(arrays))
            
        for array, scalar in zip(arrays, scalars):
            sumarray += (array*scalar)**power
        
        return(sumarray)
    
        
    @staticmethod
    def __determine_unit(unit_string_or, unit_string_target, period):
        split_or = unit_string_or.split(' ')
        split_target = unit_string_target.split(' ')
        
        if len(split_or)>1 and len(split_target)>1:
            try:
                power = int(split_target[0]) + -1 * int(split_or[0])
                scalar = 10**power * period
            except ValueError:
                print('! Unit scalar not understood. Scalar set to 1.')
                scalar = 1 * period
        else:
            print('! Unit scalar not understood. Scalar set to 1.')
            scalar = 1 * period
            
        return(scalar)



